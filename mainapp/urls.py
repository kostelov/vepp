from django.urls import re_path
import mainapp.views as mainapp

app_name = 'mainapp'

urlpatterns = [
    re_path(r'^$', mainapp.main, name='main'),
    re_path(r'^tasks/all/$', mainapp.all_task_view, name='all_tasks'),
    re_path(r'^tasks/done/$', mainapp.task_done_view, name='tasks_done'),
    re_path(r'^task/edit/(?P<task_pk>\d+)$', mainapp.task_edit_view, name='task_edit'),
    # re_path(r'^voice/$', mainapp.voicep243, name='voicep243'),
    # re_path(r'^logout/$', authapp.logout, name='logout'),
]
