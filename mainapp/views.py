from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, HttpResponseRedirect, reverse

from crmapp.models import Task, Project
from crmapp.forms import TaskEditForm


@login_required
def main(request):
    title = 'Завдання'
    user = request.user
    projects = Project.objects.filter(is_finished=False)
    tasks = Task.objects.filter(worker=user).exclude(status='done')

    context = {
        'title': title,
        'task_list': tasks,
        'project_list': projects,
    }
    return render(request, 'mainapp/index.html', context)


@login_required
def all_task_view(request):
    title = 'Всі завдання'
    projects = Project.objects.all()
    all_tasks = Task.objects.filter(worker=request.user)

    context = {
        'title': title,
        'project_list': projects,
        'task_list': all_tasks,
    }
    return render(request, 'mainapp/index.html', context)


@login_required
def task_done_view(request):
    title = 'Завершені завдання'
    projects = Project.objects.all()
    all_tasks = Task.objects.filter(worker=request.user, status='done')

    context = {
        'title': title,
        'project_list': projects,
        'task_list': all_tasks,
    }
    return render(request, 'mainapp/index.html', context)


@login_required
def task_edit_view(request, task_pk):
    title = 'Редагувати завдання'
    task = get_object_or_404(Task, pk=task_pk)
    if request.method == 'POST':
        form = TaskEditForm(request.POST, instance=task)
        if form.is_valid():
            try:
                form.save()
                return HttpResponseRedirect(reverse('main'))
            except Exception as e:
                pass
    else:
        form = TaskEditForm(instance=task)

    context = {
        'title': title,
        'form': form,
        'object': task,
    }
    return render(request, 'mainapp/task_edit.html', context)
